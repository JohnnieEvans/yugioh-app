//
//  NetworkManager.swift
//  Yugioh App
//
//  Created by Jonathan Evans on 3/15/22.
//

import Foundation
import SwiftUI

protocol NetworkManagerDelegate {
    func didFetch(cards: [YGOCard])
    func failedToFindCards()
}

class NetworkManager: ObservableObject {
    let cardInfoUrl: String = "https://db.ygoprodeck.com/api/v7/cardinfo.php"
    let setInfoUrl: String = "https://db.ygoprodeck.com/api/v7/cardsets.php"
    
    @Published var cards: [YGOCard] = []
    @Published var cardsets: [YGOSet] = []
    
    var delegate: NetworkManagerDelegate?
    
    
    // MARK: Get Cards By Name
    func getCardsBy(name: String) {
        guard let url = URL(string: cardInfoUrl + "?fname=\(name.replacingOccurrences(of: " ", with: "%20"))") else {
            return
        }
        
        let session = URLSession(configuration: .default)
        let task = session.dataTask(with: url, completionHandler: didGetCards(data:urlResponse:error:))
        task.resume()
    }
    
    func didGetCards(data: Data?, urlResponse: URLResponse?, error: Error?) {
        if let e = error {
            print(e)
            return
        }
        
        guard let d = data else {
            print("no data")
            return
        }
        
        do {
            let decoder = JSONDecoder()
            let results = try decoder.decode(Result.self, from: d)
            DispatchQueue.main.async {
                self.cards = results.data
            }
            
            delegate?.didFetch(cards: results.data)
        } catch {
            delegate?.failedToFindCards()
            return
        }
    }
    
    // MARK: Get All Cardsets
    func getAllCardsets() {
        guard let url = URL(string: setInfoUrl) else {
            return
        }
        
        let session = URLSession(configuration: .default)
        let task = session.dataTask(with: url, completionHandler: didGetCardsets(data:urlResponse:error:))
        task.resume()
    }
    
    func didGetCardsets(data: Data?, urlResponse: URLResponse?, error: Error?) {
        if let e = error {
            print(e)
            return
        }
        
        guard let d = data else {
            print("no data")
            return
        }
        
        do {
            let decoder = JSONDecoder()
            let results = try decoder.decode([YGOSet].self, from: d)
            DispatchQueue.main.async {
                self.cardsets = results
            }
        } catch {
            print(error)
            return
        }
    }
}
