//
//  YGOSet.swift
//  Yugioh App
//
//  Created by Jonathan Evans on 3/15/22.
//

import Foundation

struct YGOSet: Decodable {
    let set_name: String
    let set_code: String
    let num_of_cards: Int
    let tcg_date: String
}
