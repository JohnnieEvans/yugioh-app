//
//  YGOCard.swift
//  Yugioh App
//
//  Created by Jonathan Evans on 3/15/22.
//

import Foundation

struct YGOCard: Decodable, Identifiable {
    // MARK: Generic Properties
    let id: Int
    let name: String
    let type: String
    let desc: String
    let race: String
    let archetype: String?
    let card_sets: [YGOCardSet]?
    let card_images: [YGOCardImage]
    let card_prices: [YGOCardPrice]
    let banlist_info: YGOBannedStatus?
    
    // MARK: Monster Properties
    let atk: Int?
    let def: Int?
    let level: Int?
    let attribute: String?
    
    // MARK: Link Monster Properties
    let linkval: Int?
    let linkmarkers: [String]?
    
    // MARK: Pendulum Monster Properties
    let scale: Int?
}

struct YGOCardSet: Decodable, Identifiable {
    let set_name: String
    let set_code: String
    let set_rarity: String
    let set_rarity_code: String
    let set_price: String
    
    var id: String {
        return set_code
    }
}

struct YGOCardImage: Decodable, Identifiable {
    let id: Int
    let image_url: String
    let image_url_small: String
}

struct YGOCardPrice: Decodable {
    let cardmarket_price: String
    let tcgplayer_price: String
    let ebay_price: String
    let amazon_price: String
    let coolstuffinc_price: String
}

struct YGOBannedStatus: Decodable {
    let ban_tcg: String?
    let ban_ocg: String?
    let ban_goat: String?
}
