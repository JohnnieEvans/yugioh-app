//
//  Constants.swift
//  Yugioh App
//
//  Created by Jonathan Evans on 3/22/22.
//

import Foundation
import SwiftUI

struct K {
    struct Colors {
        static let Background = Color("BackgroundColor")
        static let Secondary = Color("SecondaryColor")
        static let Primary = Color("PrimaryColor")
        static let Teritary = Color("TeritaryColor")
        static let Other = Color("OtherColor")
    }
}
