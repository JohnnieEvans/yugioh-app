//
//  Cowboy_for_Game_App.swift
//  Yugioh App
//
//  Created by Jonathan Evans on 3/15/22.
//

import SwiftUI

@main
struct Cowboy_for_Game_App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
                .preferredColorScheme(.dark)
        }
    }
}
