//
//  SearchResults.swift
//  Yugioh App
//
//  Created by Jonathan Evans on 3/15/22.
//

import SwiftUI

struct SearchResults: View {
    let cards: [YGOCard]
    @State private var showCardView: Bool = false
    @State private var selectedCard: YGOCard? = nil
    @State private var gridView: Bool = false
    
    @ViewBuilder var body: some View {
        ScrollView {
            
            LazyVStack(alignment: .leading, spacing: 10) {
                ForEach(cards) { card in
                    Button(action: { selectedCard = card }) {
                        ZStack (alignment: .leading) {
                            RoundedRectangle(cornerRadius: 10)
                                .fill(K.Colors.Primary)
                                .shadow(color: .black, radius: 10, x: 0, y: 5)
                            HStack {
                                AsyncImage(url: URL(string: card.card_images[0].image_url)) { image in
                                    image
                                        .resizable()
                                        .scaledToFit()
                                } placeholder: {
                                    Image("cardback")
                                        .resizable()
                                        .scaledToFit()
                                }
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 75)
                                .padding()
                                Spacer()
                                
                                VStack(alignment: .trailing) {
                                    Text(card.name)
                                        .padding([.top, .trailing])
                                        .foregroundColor(K.Colors.Background)
                                        .font(.system(size: 15))
                                        .multilineTextAlignment(.trailing)
                                    Divider()
                                    Text(card.type)
                                        .foregroundColor(K.Colors.Background)
                                        .padding(.trailing)
                                        .font(.system(size: 10))
                                    
                                    Spacer()
                                    
                                    HStack(spacing: 5) {
                                        Image(systemName: "dollarsign.circle")
                                            .foregroundColor(K.Colors.Background)
                                        Text(cheapestPrice(priceArray: card.card_prices))
                                            .foregroundColor(K.Colors.Background)
                                            .font(.system(size: 12))
                                    }
                                    .padding([.bottom, .trailing])
                                }
                            }
                        }
                    }
                    .listRowBackground(K.Colors.Secondary)
                    .foregroundColor(K.Colors.Primary)
                }
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
            .padding()
            .sheet(item: $selectedCard, onDismiss: { selectedCard = nil }) { card in
                CardView(card: card)
            }
        }
        .background {
            K.Colors.Background
                .ignoresSafeArea()
        }
    }
    
    func cheapestPrice(priceArray: [YGOCardPrice]) -> String {
        var cheapestPrice: String = priceArray.first?.tcgplayer_price ?? "No Price Found"
        
        priceArray.forEach { ygoCardPrice in
            if ygoCardPrice.tcgplayer_price < cheapestPrice {
                cheapestPrice = ygoCardPrice.tcgplayer_price
            }
        }
        
        return cheapestPrice
    }
}

struct SearchResults_Previews: PreviewProvider {
    static var previews: some View {
        SearchResults(cards: testResults)
    }
}
