//
//  CardView.swift
//  Yugioh App
//
//  Created by Jonathan Evans on 3/15/22.
//

import SwiftUI

struct CardView: View {
    let card: YGOCard
    
    @ViewBuilder var body: some View {
        ScrollView {
            ZStack {
                Color(UIColor.lightGray)
                    .ignoresSafeArea()
                
                VStack {
                    AsyncImage(url: URL(string: card.card_images[0].image_url)) { image in
                        image
                            .resizable()
                            .scaledToFit()
                    } placeholder: {
                        Image("cardback")
                            .resizable()
                            .scaledToFit()
                    }
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 300)
                    .shadow(color: .black, radius: 10, x: 0, y: 10)
                    .padding()
                    
                    CardDetails(card: card)
                    
                    if let sets = card.card_sets {
                        SetDetails(sets: sets)
                    }
                    
                    Spacer()
                }
            }
            .foregroundColor(.black)
        }.ignoresSafeArea()
    }
}

struct CardDetails: View {
    let card: YGOCard
    
    @ViewBuilder var body: some View {
            VStack(alignment: .leading, spacing: 0) {
                GenericCardDetails(card: card)
                MonsterSpecificDetails(card: card)
                
                VStack(alignment: .leading) {
                    if let tcg_status = card.banlist_info?.ban_tcg {
                        Text("TCG Status - \(tcg_status)")
                    } else {
                        Text("TCG Status - Unlimited")
                    }
                    
                    if let ocg_status = card.banlist_info?.ban_ocg {
                        Text("OCG Status - \(ocg_status)")
                    } else {
                        Text("OCG Status - Unlimited")
                    }
                    
                    if let goat_status = card.banlist_info?.ban_goat {
                        Text("GOAT Status - \(goat_status)")
                    }
                    
                }
                .padding()
            }
            .background {
                RoundedRectangle(cornerRadius: 20)
                    .fill(.white)
            }.padding()
    }
}

struct GenericCardDetails: View {
    let card: YGOCard
    
    var body: some View {
        Group {
            Text(card.name)
                .font(.title)
                .padding()
            Divider()
            
            Text(card.type + " - " + card.race)
                .padding()
            Divider()
            
            Text(card.desc)
                .fixedSize(horizontal: false, vertical: true)
                .padding()
            Divider()
        }
    }
}

struct MonsterSpecificDetails: View {
    let card: YGOCard
    
    @ViewBuilder var body: some View {
        Group {
            if let attack = card.atk {
                Text("Atk - \(attack) " + "\(card.def != nil ? "Def - \(card.def!)" : "")")
                    .padding()
                Divider()
            }
            
            if let scale = card.scale {
                Text("Pendulum Scale - \(scale)")
                    .padding()
                Divider()
            }
            
            if let linkval = card.linkval, let linkmarkers = card.linkmarkers {
                Text("Link - \(linkval)")
                    .padding()
                Divider()
                
                Text("Link Marker(s)")
                    .font(.subheadline)
                HStack(alignment: .top) {
                    ForEach(linkmarkers, id: \.self) {marker in
                        Text(marker + " ")
                    }
                }.padding()
                Divider()
            }
        }
    }
}

struct SetDetails: View {
    let sets: [YGOCardSet]
    
    var body: some View {
            VStack {
                ForEach(sets) { item in
                    HStack {
                        Text("\(item.set_code) \(item.set_rarity_code)")
                            .padding()
                        Spacer()
                        Text(item.set_price)
                            .padding()
                    }.foregroundColor(.black)
                    Divider()
                }
            }
            .background {
                RoundedRectangle(cornerRadius: 20)
                    .fill(.white)
            }
            .padding()
    }
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView(card: standardMonster)
    }
}
