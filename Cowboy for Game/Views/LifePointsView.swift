//
//  LifePointsView.swift
//  Yugioh App
//
//  Created by Jonathan Evans on 3/23/22.
//

import SwiftUI

struct LifePointsView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var leftLife: Int = 8000
    @State var rightLife: Int = 8000
    @State var showCalculator: Bool = false
    
    var body: some View {
        GeometryReader { metrics in
            HStack(alignment: .center) {
                Button {
                    showCalculator = true
                } label: {
                Rectangle()
                    .frame(width: metrics.size.width * 0.40)
                    .foregroundColor(.clear)
                    .overlay {
                        Text(String(leftLife))
                            .font(Font.custom("RedHatMono-SemiBold", size: 80))
                    }
                }
                ZStack {
                    Rectangle()
                        .fill(K.Colors.Other)
                        .frame(width: 2)
                        .ignoresSafeArea()
                    
                    Menu {
                        Button("Reset") {
                            self.leftLife = 800
                            self.rightLife = 8000
                        }
                        Button("End Game", action: exitGame)
                    } label: {
                        ZStack {
                            Circle()
                                .fill(K.Colors.Other)
                                .frame(width: 30)
                            Image(systemName: "info.circle")
                                .resizable()
                                .foregroundColor(K.Colors.Secondary)
                                .frame(width: 30, height: 30)
                        }
                    }
                }
                Rectangle()
                    .frame(width: metrics.size.width * 0.40)
                    .foregroundColor(.clear)
                    .overlay {
                        Text(String(rightLife))
                            .font(Font.custom("RedHatMono-SemiBold", size: 80))
                    }
                NavigationLink(destination: CalculatorView(lifePoints: $leftLife), isActive: $showCalculator) {}
                .hidden()
            }
            .foregroundColor(K.Colors.Primary)
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .background {
                K.Colors.Background
                    .ignoresSafeArea()
            }
        }
        .onAppear {
            AppDelegate.orientationLock = UIInterfaceOrientationMask.landscapeLeft
            UIDevice.current.setValue(UIInterfaceOrientation.landscapeLeft.rawValue, forKey: "orientation")
            UIViewController.attemptRotationToDeviceOrientation()
        }
        .onDisappear {
            AppDelegate.orientationLock = UIInterfaceOrientationMask.portrait
            UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
            UIViewController.attemptRotationToDeviceOrientation()
        }
        .navigationBarHidden(true)
    }
    
    func exitGame() {
        presentationMode.wrappedValue.dismiss()
    }
}

struct LifePointsView_Previews: PreviewProvider {
    static var previews: some View {
        LifePointsView()
            .previewInterfaceOrientation(.landscapeLeft)
    }
}
