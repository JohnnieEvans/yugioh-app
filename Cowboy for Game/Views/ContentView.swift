//
//  ContentView.swift
//  Yugioh App
//
//  Created by Jonathan Evans on 3/15/22.
//

import SwiftUI

struct ContentView: View, NetworkManagerDelegate {
    @ObservedObject private var networkManager = NetworkManager()
    
    @State private var searchString: String = ""
    @State private var showResults: Bool = false
    @State private var cards: [YGOCard] = []
    @State private var showCardView: Bool = false
    @State private var selectedCard: YGOCard? = nil
    @State private var noCardFound: Bool = false
    
    @ViewBuilder var body: some View {
        NavigationView {
            VStack {
                Text("Cowboy for Game")
                    .font(.title)
                    .foregroundColor(K.Colors.Primary)
                
                HStack {
                    Button {
                        searchCard()
                    } label: {
                        Image(systemName: "magnifyingglass")
                    }
                    TextField("Card To Search", text: $searchString)
                }
                .padding(.vertical, 10)
                .overlay(Rectangle().frame(height: 2).padding(.top, 35))
                .foregroundColor(K.Colors.Primary)
                .padding(10)
                .onSubmit(searchCard)
                .alert("No cards found with that name", isPresented: $noCardFound) {}
                
                NavigationLink(destination: SearchResults(cards: cards), isActive: $showResults) {}
                .hidden()
                
                NavigationLink(destination: LifePointsView()) {
                    Text("Play Game")
                }
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .center)
            .background {
                K.Colors.Background
                    .ignoresSafeArea()
            }
            .navigationBarHidden(true)
        }
        .onAppear {
            self.networkManager.delegate = self
        }
        .sheet(item: $selectedCard, onDismiss: { selectedCard = nil }) { card in
            CardView(card: card)
        }
    }
    
    func searchCard() {
        networkManager.getCardsBy(name: searchString)
    }
    
    func didFetch(cards: [YGOCard]) {
        self.cards = cards
        
        if self.cards.count == 1 {
            self.selectedCard = self.cards.first
        } else {
            self.showResults = true
        }
    }
    
    func failedToFindCards() {
        noCardFound = true
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
