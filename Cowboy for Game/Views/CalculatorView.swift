//
//  CalculatorView.swift
//  Yugioh App
//
//  Created by Jonathan Evans on 3/24/22.
//

import SwiftUI

enum CalcButton: String {
    case one = "1"
    case two = "2"
    case three = "3"
    case four = "4"
    case five = "5"
    case six = "6"
    case seven = "7"
    case eight = "8"
    case nine = "9"
    case zero = "0"
    case add = "plus"
    case subtract = "minus"
    case divide = "divide"
    case multiply = "multiply"
    case equal = "equal"
    case clear = "AC"
    
    var buttonColor: Color {
        switch self {
        case .add, .subtract, .multiply, .divide, .clear:
            return K.Colors.Secondary
        case .equal:
            return K.Colors.Teritary
        default:
            return K.Colors.Primary
        }
    }
    
    var fontColor: Color {
        switch self {
        case .add, .subtract, .multiply, .divide, .clear:
            return K.Colors.Primary
        default:
            return K.Colors.Background
        }
    }
}

enum Operation {
    case add, subtract, multiply, divide, none
}

struct CalculatorView: View {
    @Binding var lifePoints: Int
    @State var value = "0"
    @State var runningNumber = 0
    @State var currentOperation: Operation = .none
    let buttons: [[CalcButton]] = [
        [.clear, .divide],
        [.seven, .eight, .nine, .multiply],
        [.four, .five, .six, .subtract],
        [.one, .two, .three, .add],
        [.zero, .equal],
    ]
    
    @ViewBuilder var body: some View {
        VStack {
            Spacer()
            
            HStack {
                Spacer()
                Text(value)
                    .font(Font.custom("RedHatMono-SemiBold", size: 60))
            }
            .padding()
            
            ForEach(buttons, id: \.self) { row in
                HStack {
                    ForEach(row, id: \.self) { item in
                        Button {
                            self.didTap(button: item)
                        } label: {
                            if [.equal,.multiply,.divide,.add,.subtract].contains(item) {
                                Image(systemName: item.rawValue)
                                    .frame(
                                        width: self.buttonWidth(item: item),
                                        height: self.buttonHeight()
                                    )
                                    .background(item.buttonColor)
                                    .foregroundColor(item.fontColor)
                                    .cornerRadius(self.buttonWidth(item: item)/2)
                            } else {
                                Text(item.rawValue)
                                    .frame(
                                        width: self.buttonWidth(item: item),
                                        height: self.buttonHeight()
                                    )
                                    .background(item.buttonColor)
                                    .foregroundColor(item.fontColor)
                                    .cornerRadius(self.buttonWidth(item: item)/2)
                            }
                        }
                    }
                }
            }
        }
        .font(Font.custom("RedHatMono-SemiBold", size: 20))
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .foregroundColor(K.Colors.Primary)
        .background {
            K.Colors.Background
                .ignoresSafeArea()
        }
        
    }
    
    func didTap(button: CalcButton) {
        switch button {
        case .add, .subtract, .multiply, .divide, .equal:
            if button == .add {
                self.currentOperation = .add
                self.runningNumber = Int(self.value) ?? 0
            }
            else if button == .subtract {
                self.currentOperation = .subtract
                self.runningNumber = Int(self.value) ?? 0
            }
            else if button == .multiply {
                self.currentOperation = .multiply
                self.runningNumber = Int(self.value) ?? 0
            }
            else if button == .divide {
                self.currentOperation = .divide
                self.runningNumber = Int(self.value) ?? 0
            }
            else if button == .equal {
                let runningValue = self.runningNumber
                let currentValue = Int(self.value) ?? 0
                switch self.currentOperation {
                case .add: self.value = "\(runningValue + currentValue)"
                case .subtract: self.value = "\(runningValue - currentValue)"
                case .multiply: self.value = "\(runningValue * currentValue)"
                case .divide: self.value = "\(runningValue / currentValue)"
                case .none:
                    break
                }
            }
            
            if button != .equal {
                self.value = "0"
            }
        case .clear:
            self.value = "0"
        default:
            let number = button.rawValue
            if self.value == "0" {
                value = number
            }
            else {
                self.value = "\(self.value)\(number)"
            }
        }
    }
    
    func buttonWidth(item: CalcButton) -> CGFloat {
        if item == .zero || item == .clear {
            return ((UIScreen.main.bounds.width - (4*12)) / 4) * 3
        }
        return (UIScreen.main.bounds.width - (5*12)) / 4
    }
    
    func buttonHeight() -> CGFloat {
        return (UIScreen.main.bounds.width - (5*12)) / 4
    }
}

struct CalculatorView_Previews: PreviewProvider {
    @State static var lifePoints = 8000
    
    static var previews: some View {
        CalculatorView(lifePoints: $lifePoints)
    }
}
